import Vue from 'vue'
import Vuex from 'vuex'
import MediaContents from './modules/mediacontents'
import Contents from './modules/contents'
import Pages from './modules/pages'
import User from './modules/user'
import Files from './modules/files'
import Members from './modules/members'
import Documents from './modules/documents'
import Videos from './modules/videos'
import Banners from './modules/banners'
import Auth from './modules/auth'
import News from './modules/news'

Vue.use(Vuex)

const debug = process.env.NODE_ENV !== 'production'

export const createStore = () => new Vuex.Store({
  modules: {
    MediaContents,
    Contents,
    Pages,
    User,
    Members,
    Files,
    Documents,
    Videos,
    Banners,
    Auth,
    News
  },
  strict: debug
})

export default createStore
