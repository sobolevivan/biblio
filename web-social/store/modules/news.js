import api from '../../api/api'

// initial state
const state = {
  news: [],
  newss: null,
  newsCount: 0
}

// getters
const getters = {
  news: (state) => {
    return state.news
  },
  newss: (state) => {
    return state.newss
  },
  newsCount: (state) => {
    return state.newsCount
  }
}

// actions
const actions = {
  getListNews ({commit}, params) {
    return api.getListNews(params).then((banners) => {
      commit('setNews', banners)
    })
  },
  getNews ({commit}, params) {
    return api.getNews(params).then((banners) => {
      commit('setNewss', banners)
    })
  },
  getNewsCount ({commit}, params) {
    return api.getNewsCount(params).then((banners) => {
      commit('setNewsCount', banners)
      return Promise.resolve(banners)
    })
  },
  createNews ({commit}, params) {
    return api.createNews(params).then((banner) => {
      commit('setNewss', banner)
    })
  },
  deleteNews ({commit}, params) {
    return api.deleteNews(params).then((banners) => {
      commit('setNewss', banners)
    })
  },
  editNews ({commit}, params) {
    return api.editNews(params).then((banners) => {
      commit('setNewss', banners)
    })
  }
}

// mutations
const mutations = {
  setNews (state, banners) {
    state.news = banners
  },
  setNewss (state, item) {
    state.newss = item
  },
  setNewsCount (state, item) {
    state.newsCount = item
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}
