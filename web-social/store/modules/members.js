import api from '../../api/api'

// initial state
const state = {
  users: [],
  user: null,
  auth: null
}

// getters
const getters = {
  users: (state) => {
    return state.users
  },
  user: (state) => {
    return state.user
  }
}

// actions
const actions = {
  getUser ({commit}, params) {
    return api.getUser(params).then((users) => {
      commit('setUsers', users)
    })
  },
  getListUser ({commit}, params) {
    return api.getListUser(params).then((users) => {
      commit('setUsers', users)
    })
  },
  createUser ({commit}, params) {
    return api.createUser(params).then((user) => {
      commit('setUser', user)
    })
  },
  deleteUser ({commit}, params) {
    return api.deleteUser(params).then((users) => {
      commit('setUsers', users)
    })
  },
  editUser ({commit}, params) {
    return api.editUser(params).then((users) => {
      commit('setUsers', users)
    })
  }
}

// mutations
const mutations = {
  setUsers (state, users) {
    state.users = users
  },
  setUser (state, user) {
    state.user = user
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}
