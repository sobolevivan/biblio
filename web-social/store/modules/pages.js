import api from '../../api/api'

// initial state
const state = {
  pages: [],
  page: null
}

// getters
const getters = {
  pages: (state) => {
    return state.pages
  },
  page: (state) => {
    return state.page
  }
}

// actions
const actions = {
  getListPage ({commit}, params) {
    return api.getListPage(params).then((pages) => {
      commit('setPages', pages)
    })
  },
  createPage ({commit}, params) {
    return api.createPage(params).then((page) => {
      commit('setPage', page)
    })
  },
  deletePage ({commit}, params) {
    return api.deletePage(params).then((pages) => {
      commit('setPages', pages)
    })
  },
  editPage ({commit}, params) {
    return api.editPage(params).then((pages) => {
      commit('setPages', pages)
    })
  }
}

// mutations
const mutations = {
  setPages (state, pages) {
    state.pages = pages
  },
  setPage (state, item) {
    state.page = item
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}
