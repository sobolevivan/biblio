import api from '../../api/api'

// initial state
const state = {
  auth: false,
  username: '',
  users: [],
  books: [],
  bron: []
}

// getters
const getters = {
  auth: (state) => {
    return state.auth
  },
  users: (state) => {
    return state.users
  },
  username: (state) => {
    return state.username
  },
  books: (state) => {
    return state.books
  },
  bron: (state) => {
    return state.bron
  }
}

// actions
const actions = {
  getAuth ({commit}, params) {
    if (params.bool) {
      commit('login')
    } else {
      commit('logout')
    }
  },
  setUser ({commit}, params) {
    commit('setUsername', params.login)
  },
  getListUser ({commit}, params) {
    return api.getListUser(params).then((users) => {
      commit('setUsers', users)
    })
  },
  reg ({commit}, params) {
    return api.reg(params).then((users) => {
      commit('setUsers', users)
    })
  },
  getUserBooks ({commit}, params) {
    return api.getUserBooks(params).then((users) => {
      commit('setBooks', users)
    })
  },
  delBooks ({commit}, params) {
    return api.delBooks(params).then((users) => {
      commit('setBooks', (state.books || []).filter(item => item.id !== params.id))
    })
  },
  addBooks ({commit}, params) {
    return api.addBooks(params).then((users) => {
      commit('setBooks', users)
    })
  },
  addBron ({commit}, params) {
    return api.addBron(params).then((users) => {
      commit('setBron', users)
    })
  },
  plusBron ({commit}, params) {
    return api.plusBron(params).then((users) => {
      commit('setBron', users)
    })
  },
  plusBook ({commit}, params) {
    return api.plusBook(params).then((users) => {
      commit('setBooks', users)
    })
  },
  getListBooks ({commit}, params) {
    return api.getListBooks(params).then((users) => {
      commit('setBooks', users)
    })
  },
  getUserBron ({commit}, params) {
    return api.getUserBron(params).then((users) => {
      commit('setBron', users)
    })
  },
  delBron ({commit}, params) {
    return api.delBron(params).then((users) => {
      commit('setBron', (state.bron || []).filter(item => item.id !== params.id))
    })
  },
  getListBron ({commit}, params) {
    return api.getListBron(params).then((users) => {
      commit('setBron', users)
    })
  },
  editUser ({commit}, params) {
    return api.editUser(params).then((users) => {
      commit('setUsers', users)
    })
  }
}

// mutations
const mutations = {
  login (state) {
    state.auth = true
  },
  logout (state) {
    state.auth = false
  },
  setUsers (state, item) {
    state.users = item
  },
  setUsername (state, item) {
    state.username = item
  },
  setBooks (state, item) {
    state.books = item
  },
  setBron (state, item) {
    state.bron = item
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}
