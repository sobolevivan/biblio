import api from '../../api/api'

// initial state
const state = {
  files: [],
  file: null,
  storages: [],
  filesCount: 0
}

// getters
const getters = {
  files: (state) => {
    return state.files
  },
  storages: (state) => {
    return state.storages
  },
  file: (state) => {
    return state.file
  },
  filesCount: (state) => {
    return state.filesCount
  }
}

// actions
const actions = {
  uploadFile ({commit}, params) {
    return api.uploadFile(params).then((file) => {
      commit('setFile', file)
      return Promise.resolve(file)
    })
  },
  getFileList ({commit}, params) {
    return api.getFileList(params).then((files) => {
      commit('setFiles', files)
    })
  },
  getStorageList ({commit}, params) {
    return api.getStorageList(params).then((storages) => {
      commit('setStorages', storages)
    })
  },
  deleteFile ({commit}, params) {
    return api.deleteFile(params).then((files) => {
      commit('setFiles', files)
    })
  },
  getFilesCount ({commit}, params) {
    return api.getFileList(params).then((filesCount) => {
      commit('setFilesCount', filesCount.length)
      return Promise.resolve(filesCount.length)
    })
  },
  delFile ({commit}, params) {
    var files = state.files.slice()
    let index = files.findIndex(el => el.name === params.params)
    files.splice(index, 1)
    commit('delFiles', files)
  }
}

// mutations
const mutations = {
  setFiles (state, files) {
    state.files = files
  },
  setFilesCount (state, filesCount) {
    state.filesCount = filesCount
  },
  setStorages (state, storages) {
    state.storages = storages
  },
  delFiles (state, files) {
    state.files = files
  },
  setFile (state, file) {
    state.file = file
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}
