import api from '../../api/api'

// initial state
const state = {
  contents: [],
  content: null
}

// getters
const getters = {
  contents: (state) => {
    return state.contents
  },
  content: (state) => {
    return state.content
  }
}

// actions
const actions = {
  getListContent ({commit}, params) {
    return api.getListContent(params).then((contents) => {
      commit('setContents', contents)
    })
  },
  getContent ({commit}, params) {
    return api.getContent(params).then((content) => {
      commit('setContent', content)
    })
  },
  createContent ({commit}, params) {
    return api.createContent(params).then((content) => {
      commit('setContent', content)
    })
  },
  deleteContent ({commit}, params) {
    return api.deleteContent(params).then(() => {
      var res = (this.getters.contents || []).filter(item => item.id !== params.id)
      commit('setContents', res)
    })
  },
  editContent ({commit}, params) {
    return api.editContent(params).then((content) => {
      commit('setContent', content)
    })
  }
}

// mutations
const mutations = {
  setContents (state, contents) {
    state.contents = contents
  },
  setContent (state, content) {
    state.content = content
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}
