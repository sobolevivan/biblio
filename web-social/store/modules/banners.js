import api from '../../api/api'

// initial state
const state = {
  banners: [],
  banner: null,
  bannerCount: 0
}

// getters
const getters = {
  banners: (state) => {
    return state.banners
  },
  banner: (state) => {
    return state.banner
  },
  bannerCount: (state) => {
    return state.bannerCount
  }
}

// actions
const actions = {
  getListBanner ({commit}, params) {
    return api.getListBanner(params).then((banners) => {
      commit('setBanners', banners)
    })
  },
  getBannerCount ({commit}, params) {
    return api.getBannerCount(params).then((banners) => {
      commit('setBannerCount', banners)
      return Promise.resolve(banners)
    })
  },
  createBanner ({commit}, params) {
    return api.createBanner(params).then((banner) => {
      commit('setBanner', banner)
    })
  },
  deleteBanner ({commit}, params) {
    return api.deleteBanner(params).then((banners) => {
      commit('setBanner', banners)
    })
  },
  editBanner ({commit}, params) {
    return api.editBanner(params).then((banners) => {
      commit('setBanners', banners)
    })
  }
}

// mutations
const mutations = {
  setBanners (state, banners) {
    state.banners = banners
  },
  setBanner (state, item) {
    state.banner = item
  },
  setBannerCount (state, item) {
    state.bannerCount = item
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}
