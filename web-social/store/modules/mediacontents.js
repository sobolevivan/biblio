import api from '../../api/api'

// initial state
const state = {
  mediaContents: [],
  mediaContent: null,
  biblios: []
}

// getters
const getters = {
  mediaContents: (state) => {
    return state.mediaContents
  },
  mediaContent: (state) => {
    return state.mediaContent
  },
  biblios: (state) => {
    return state.biblios
  }
}

// actions
const actions = {
  getListMediaContent ({commit}, params) {
    return api.getListMediaContent(params).then((mediaContents) => {
      console.log('getListMediaContent:', mediaContents)
      commit('setMediaContents', mediaContents)
    })
  },
  getMediaContentOfContent ({commit}, params) {
    return api.getMediaContentOfContent(params).then((mediaContents) => {
      commit('setMediaContents', mediaContents)
    })
  },
  getListBiblios ({commit}, params) {
    return api.getListMediaContent(params).then((mediaContents) => {
      commit('setBiblios', mediaContents)
    })
  },
  getMediaContent ({commit}, params) {
    return api.getMediaContent(params).then((mediaContent) => {
      commit('setMediaContent', mediaContent)
    })
  },
  createMediaContent ({commit}, params) {
    return api.createMediaContent(params).then((mediaContent) => {
      commit('setMediaContent', mediaContent)
    })
  },
  deleteMediaContent ({commit}, params) {
    return api.deleteMediaContent(params).then((mediaContent) => {
      commit('setMediaContent', mediaContent)
    })
  },
  editMediaContent ({commit}, params) {
    return api.editMediaContent(params).then((mediaContent) => {
      commit('setMediaContent', mediaContent)
    })
  }
}

// mutations
const mutations = {
  setMediaContents (state, mediaContents) {
    state.mediaContents = mediaContents
  },
  setMediaContent (state, item) {
    state.mediaContent = item
  },
  setBiblios (state, biblios) {
    state.biblios = biblios
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}
