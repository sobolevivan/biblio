export default {
  hostimage: 'http://localhost:3001/images',
  host: 'http://localhost:3001',
  apihost: 'http://localhost:3001/api/',
  storage: 'http://localhost:3001/api/storages',
  content: {
    newsId: '5d8249d9ec229618b0d481eb',
    galleryId: '5d82f63ee6c7cc22d4e51c39',
    nkoId: '5d8a8398507c1913751a5363',
    grantId: '5d92591d05d2904e908f47a2',
    lawId: '5db09dc747006d5bce942678',
    schoolId: '5db09d8847006d5bce942677',
    biblioId: '5d925a1c05d2904e908f47a4',
    aboutId: '5dbb2d1c27f30c17a9264caa'
  }
}
