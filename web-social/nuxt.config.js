module.exports = {

  server: {
    port: 8000 // default: 3000
  },
  /*
  ** Headers of the page
  */
  head: {
    title: 'starter',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'Nuxt.js project' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      { href: 'https://fonts.googleapis.com/css?family=Noto+Serif+TC', rel: 'stylesheet' },
      { href: 'https://fonts.googleapis.com/css?family=PT+Sans+Narrow', rel: 'stylesheet' },
      { href: 'https://fonts.googleapis.com/css?family=Fira+Sans+Condensed:100,400,700&amp;subset=cyrillic', rel: 'stylesheet' },
      { href: 'https://fonts.googleapis.com/css?family=Yanone+Kaffeesatz:200,300,400,700&display=swap&subset=cyrillic', rel: 'stylesheet' }
    ]
  },
  /*
  ** bootstrap CSS
  */
  modules: [
    'bootstrap-vue/nuxt'
  ],
  /*
  ** Global CSS
  */
  css: ['~/assets/css/main.scss'],
  /*
  ** Add axios globallys
  */
  plugins: ['~plugins/vue-awesome.js', {src: '~plugins/vue-gallery.js', ssr: false}, {src: '~/plugins/vue-carousel.js', ssr: false}],
  build: {
    vendor: ['axios', 'vue-awesome'],
    /*
    ** Run ESLINT on save
    */
    extend (config, ctx) {
      if (ctx.isDev && ctx.isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
    }
  }
  /* ,serverMiddleware: [
    // API middleware
    '~/api/index.js'
  ] */
}
