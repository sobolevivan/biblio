import Vue from 'vue'
import { library } from '@fortawesome/fontawesome-svg-core'
import { faAngleDoubleRight } from '@fortawesome/free-solid-svg-icons'
import { faFacebook, faInstagram, faVk } from '@fortawesome/free-brands-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

library.add(faAngleDoubleRight, faFacebook, faInstagram, faVk)

Vue.component('font-awesome-icon', FontAwesomeIcon)
Vue.config.productionTip = false
