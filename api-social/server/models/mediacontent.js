module.exports = function(MediaContent) {
  MediaContent.observe('after save', function(ctx, next) {
    var Storage = MediaContent.app.models.Storage;

    if (ctx.instance) {
      var name = ctx.instance.id.toString();

      Storage.getContainers(function(err, containers) {
        if (err) {
          return next(err);
        }

        if (!containers.some(function(e) {
          return e.name === name;
        })) {
          Storage.createContainer({
            name: name,
          }, function(err, c) {
            if (err) {
              return next(err);
            }
          });
        }
      });
    };

    next();
  });

  MediaContent.observe('before delete', function(ctx, next) {
    var Storage = MediaContent.app.models.Storage;

    if (ctx.where && ctx.where.id) {
      var name = ctx.where.id.toString();

      Storage.getContainers(function(err, containers) {
        if (err) {
          return next(err);
        }

        if (containers.some(function(e) {
          return e.name === name;
        })) {
          Storage.destroyContainer(name, function(err, c) {
            if (err) {
              return next(err);
            }
          });
        }
      });
    };

    next();
  });
};
