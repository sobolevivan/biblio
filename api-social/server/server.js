/* eslint-disable no-undef */
/* eslint-disable no-redeclare */
/* eslint-disable no-multi-spaces */
/* eslint-disable max-len */
/* eslint-disable no-constant-condition */

var loopback = require('loopback');
var boot = require('loopback-boot');
var devwebpackconf = require('./../admin/webpack.dev.config');
var webpack = require('webpack');

var app = module.exports = loopback();

app.start = function() {
  if (true) {
    const compiler = webpack(devwebpackconf);

    compiler.watch({
      aggregateTimeout: 300,
    },
    (err, stats) => {
      if (err) {
        console.error(`ERROR OCCURED while webpack watching: ${err.stack} | ${err}`);
        if (err.details) {
          console.log(err.details);
        }
      } else {
        const info = stats.toJson();

        if (stats.hasErrors()) {
          console.error(info.errors);
        }
        if (stats.hasWarnings()) {
          console.error(info.warnings);
        }
        console.log(`STATS watching webpack: ${stats}`);
      }
    });
  }

  return app.listen(function() {
    app.emit('started');
    var baseUrl = app.get('url').replace(/\/$/, '');
    console.log('Web server listening at: %s', baseUrl);
    if (app.get('loopback-component-explorer')) {
      var explorerPath = app.get('loopback-component-explorer').mountPath;
      console.log('Browse your REST API at %s%s', baseUrl, explorerPath);
    }
  });
};

// Bootstrap the application, configure models, datasources and middleware.
// Sub-apps like REST API are mounted via boot scripts.
boot(app, __dirname, function(err) {
  if (err) throw err;

  // start the server if `$ node server.js`
  if (require.main === module)   { app.start(); }
});
