'use strict';
const path = require('path');
const staticHtml = './../../admin/index.html'

module.exports = function(server) {
  var router = server.loopback.Router();

  router.get('/', function(req, res, next) {
    res.sendFile(path.resolve(__dirname, staticHtml));
  });

  router.get('/contents', function(req, res, next) {
    res.sendFile(path.resolve(__dirname, staticHtml));
  });

  router.get('/contents/edit/:id', function(req, res, next) {
    res.sendFile(path.resolve(__dirname, staticHtml));
  });

  router.get('/boxcontent/edit/:id', function(req, res, next) {
    res.sendFile(path.resolve(__dirname, staticHtml));
  });

  router.get('/boxcontents', function(req, res, next) {
    res.sendFile(path.resolve(__dirname, staticHtml));
  });

  router.get('/contents/new', function(req, res, next) {
    res.sendFile(path.resolve(__dirname, staticHtml));
  });

  router.get('/contents/entry/:contentSlug/:command?/:id?', function(req, res, next) {
    res.sendFile(path.resolve(__dirname, staticHtml));
  });

  router.get('/contents/[^/]+', function(req, res, next) {
    res.sendFile(path.resolve(__dirname, staticHtml));
  });

  router.get('/login', function(req, res, next) {
    res.sendFile(path.resolve(__dirname, staticHtml));
  });

  router.get('/createPage', function(req, res, next) {
    res.sendFile(path.resolve(__dirname, staticHtml));
  });

  router.get('/user/edit/:id', function(req, res, next) {
    res.sendFile(path.resolve(__dirname, staticHtml));
  });

  router.get('/users', function(req, res, next) {
    res.sendFile(path.resolve(__dirname, staticHtml));
  });

  router.get('/docs', function(req, res, next) {
    res.sendFile(path.resolve(__dirname, staticHtml));
  });

  router.get('/createdoc/:id', function(req, res, next) {
    res.sendFile(path.resolve(__dirname, staticHtml));
  });

  router.get('/createdoc', function(req, res, next) {
    res.sendFile(path.resolve(__dirname, staticHtml));
  });

  router.get('/files', function(req, res, next) {
    res.sendFile(path.resolve(__dirname, staticHtml));
  });

  router.get('/pages', function(req, res, next) {
    res.sendFile(path.resolve(__dirname, staticHtml));
  });

  router.get('/profile', function(req, res, next) {
    res.sendFile(path.resolve(__dirname, staticHtml));
  });

  router.get('/search', function(req, res, next) {
    res.sendFile(path.resolve(__dirname, staticHtml));
  });

  router.get('/alltextcontents', function(req, res, next) {
    res.sendFile(path.resolve(__dirname, staticHtml));
  });

  server.use(router);
};
