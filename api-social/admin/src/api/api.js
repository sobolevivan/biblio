import axios from 'axios';
import config from './../config';
import moment from 'moment';

export default {
  /**
   * get List TextContent
   * @param {*} params
   * @return {promise} promise
   */
  getListTextContent(params, cb) {
    const filter = params ? '?filter=' + JSON.stringify(params) : ''
    return axios({
        method: 'get',
        url: `${config.apihost}/TextContent${filter}`,
      })
      .then(function (response) {
        return Promise.resolve(response.data);
      })
  },

  /**
   * Create TextContent
   * @param {*} params
   * @return {promise} promise
   */
  createTextContent(params) {
    return axios({
        method: 'post',
        url: `${config.apihost}/TextContent`,
        data: {
          'name': params.name,
          'text': params.text,
          'shortText': params.shortText,
          'dateCreated': moment(new Date().toISOString, "MM-DD-YYYY"),
          'datePublished': moment(new Date().toISOString, "MM-DD-YYYY"),
          'metaData': params.metaData,
          'contentId': params.contentId,
          'userId': params.userId,
          'tags': params.tags,
          'image': params.image,
          'backgroundColor': params.backgroundColor,
          'isPublished': false,
          'parentId': params.parentId
        }
      })
      .then(function (response) {
        return Promise.resolve(response.data);
      })
  },

  /**
   * Delete TextContent
   * @param {*} params
   * @return {promise} promise
   */
  deleteTextContent(params) {
    return axios({
      method: 'delete',
      url: `${config.apihost}/TextContent/${params.id}`,
    })
  },

  /**
   * Edit TextContent
   * @param {*} params
   * @return {promise} promise
   */
  editTextContent(params) {
    return axios({
      method: 'put',
      url: `${config.apihost}/TextContent/${params.id}`,
      data: {
        'name': params.name,
        'text': params.text,
        'shortText': params.shortText,
        'dateCreated': params.dateCreated,
        'datePublished': params.datePublished,
        'metaData': params.metaData,
        'contentId': params.contentId,
        'userId': params.userId,
        'tags': params.tags,
        'image': params.image,
        'backgroundColor': params.backgroundColor,
        'isPublished': params.isPublished,
        'parentId': params.parentId
      }
    })
  },

  /**
   * Get TextContent
   * @param {*} params
   * @return {promise} promise
   */
  getTextContent(params) {
    return axios({
      method: 'get',
      url: `${config.apihost}/TextContent/${params.id}`
    }).then(function (response) {
      return Promise.resolve(response.data);
    })
  },

  /**
   * Get TextContent Count
   * @param {*} params
   * @return {promise} promise
   */
  getTextContentCount(params) {
    const filter = params ? '?where=' + JSON.stringify(params) : ''
    return axios({
      method: 'get',
      url: `${config.apihost}/TextContent/count${filter}`
    }).then(function (response) {
      return Promise.resolve(response.data);
    });
  },

  /**
   * Get Content
   * @param {*} params
   * @return {promise} promise
   */
  getContent(params) {
    return axios({
      method: 'get',
      url: `${config.apihost}/content/${params.id}`
    }).then(function (response) {
      return Promise.resolve(response.data);
    });
  },

  /**
   * Get Content
   * @param {*} params
   * @return {promise} promise
   */
  getContentBySlug(params) {
    const filter = params ? '?filter=' + JSON.stringify(params) : ''
    return axios({
      method: 'get',
      url: `${config.apihost}/content${filter}`
    }).then(function (response) {
      return Promise.resolve(response.data);
    });
  },

  /**
   * get List Content
   * @param {*} params
   * @return {promise} promise
   */
  getListContent (params, cb) {
    const filter = params ? '?filter=' + JSON.stringify(params) : ''
    return axios({
        method: 'get',
        url: `${config.apihost}/content${filter}`
      }).then(function (response) {
        return Promise.resolve(response.data)
      })
  },

    /**
   * Get Content Count
   * @param {*} params
   * @return {promise} promise
   */
  getContentCount(params) {
    return axios({
      method: 'get',
      url: `${config.apihost}/content/count`
    }).then(function (response) {
      return Promise.resolve(response.data);
    });
  },

  /**
   * Create Content
   * @param {*} params
   * @return {promise} promise
   */
  createContent(params) {
    return axios({
        method: 'post',
        url: `${config.apihost}/content`,
        data: {
          'name': params.name,
          'slug': params.slug,
          'description': params.description,
          'type': params.type,
          'image': params.image,
          'parentId': params.parentId,
          'metaData': params.metaData
        }
      })
      .then((response) => {
        let result = {
          id: response.data.id
        };
        return Promise.resolve(result);
      })
  },

  /**
   * Delete Content
   * @param {*} params
   * @return {promise} promise
   */
  deleteContent(params) {
    return axios({
      method: 'delete',
      url: `${config.apihost}/content/${params.id}`,
    })
  },

  /**
   * Edit Content
   * @param {*} params
   * @return {promise} promise
   */
  editContent(params) {
    return axios({
      method: 'put',
      url: `${config.apihost}/content/${params.id}`,
      data: {
        'name': params.name,
        'slug': params.slug,
        'description': params.description,
        'type': params.type,
        'image': params.image,
        'parentId': params.parentId,
        'metaData': params.metaData
      }
    })
  },

  /**
   * get List Page
   * @param {*} params
   * @param {*} cb
   * @return {promise} promise
   */
  getListPage(params) {
    const filter = params ? '?filter=' + JSON.stringify(params) : ''
    return axios({
        method: 'get',
        url: `${config.apihost}/Page${filter}`,
      })
      .then(function (response) {
        return Promise.resolve(response.data);
      })
  },

  /**
   * Create Page
   * @param {*} params
   * @return {promise} promise
   */
  createPage(params, cb) {
    return axios({
        method: 'post',
        url: `${config.apihost}/Page`,
        data: {
          'name': params.name,
          'slug': params.slug,
          'description': params.description,
        }
      })
      .then(function (response) {
        let result = {
          id: response.data.id
        };
        return Promise.resolve(result);
      })
  },

  /**
   * Delete Page
   * @param {*} params
   * @return {promise} promise
   */
  deletePage(params, cb) {
    return axios({
      method: 'delete',
      url: `${config.apihost}/Page/${params.id}`,
    })
  },

  /**
   * Edit Page
   * @param {*} params
   * @return {promise} promise
   */
  editPage(params, cb) {
    return axios({
      method: 'put',
      url: `${config.apihost}/Page/${params.id}`,
      data: {
        'name': params.name,
        'slug': params.slug,
        'description': params.description
      }
    })
  },

  /**
   * Get TextContentOfContent list
   * @param {*} params
   * @return {promise} promise
   */
  getTextContentOfContent(params) {
    const filter = params.filter ? '?filter=' + JSON.stringify(params.filter) : ''
    return axios({
        method: 'get',
        url: `${config.apihost}/content/${params.id}/TextContents${filter}`,
      })
      .then(function (response) {
        return Promise.resolve(response.data);
      })
  },

  /**
   * get List User
   * @param {*} params
   * @return {promise} promise
   */
  auth(params) {
    return axios({
      method: 'post',
      url: `${config.apihost}/Members/Login`,
      data: {
        'email': params.email,
        'password': params.password,
      }
    }).then(function (response) {
      return Promise.resolve(response.data);
    });
  },

  /**
   * get List User
   * @param {*} params
   * @return {promise} promise
   */
  getUser(params) {
    return axios({
      method: 'get',
      url: `${config.apihost}/Members/${params.userId}`,
    }).then(function (response) {
      return Promise.resolve(response.data);
    });
  },

  /**
   * get List User
   * @param {*} params
   * @return {promise} promise
   */
  getListUser(params) {
    const filter = params ? '?filter=' + JSON.stringify(params) : ''
    return axios({
        method: 'get',
        url: `${config.apihost}/Members${filter}`,
      })
      .then(function (response) {
        return Promise.resolve(response.data);
      });
  },

  /**
   * Create User
   * @param {*} params
   * @return {promise} promise
   */
  createUser(params) {
    return axios({
        method: 'post',
        url: `${config.apihost}/Members`,
        data: {
          'name': params.name,
          'email': params.email,
          'password': params.password,
          'avatar': params.avatar,
        }
      })
      .then(function (response) {
        let result = {
          id: response.data.id
        };
        return Promise.resolve(result);
      });
  },

  /**
   * Delete User
   * @param {*} params
   * @return {promise} promise
   */
  deleteUser(params) {
    return axios({
      method: 'delete',
      url: `${config.apihost}/Members/${params.id}`,
    })
  },

  /**
   * Edit User
   * @param {*} params
   * @return {promise} promise
   */
  editUser(params) {
    return axios({
      method: 'patch',
      url: `${config.apihost}/Members/${params.id}`,
      data: {
        'username': params.username,
        'sites': params.sites
      }
    })
  },

  /**
   * Edit Profile
   * @param {*} params
   * @return {promise} promise
   */
  editProfile(params) {
    return axios({
      method: 'put',
      url: `${config.apihost}/Members/${params.userId}/profile`,
      data: {
        'avatar': params.avatar,
        'userId': params.userId,
      }
    }).then(function (response) {
      return Promise.resolve(response.data);
    })
  },

    /**
   * Create Profile
   * @param {*} params
   * @return {promise} promise
   */
  createProfile(params) {
    return axios({
      method: 'post',
      url: `${config.apihost}/Profile`,
      data: {
        'avatar': params.avatar,
        'userId': params.userId,
      }
    }).then(function (response) {
      return Promise.resolve(response.data);
    })
  },

  /**
   * Upload File
   * @param {*} params
   * @return {promise} promise
   */
  uploadFile(params) {
    return axios({
      method: 'post',
      url: `${config.apihost}/storages/${params.name}/upload`,
      headers: {
        'Content-Type': 'multipart/form-data',
      },
      data: params.filedata
    }).then(function (response) {
      return Promise.resolve(response.data);
    })
  },

  /**
   * Get File List
   * @param {*} params
   * @return {promise} promise
   */
  getFileList(params) {
    return axios({
        method: 'get',
        url: `${config.apihost}/storages/${params.name}/files`
      })
      .then(function (response) {
        return Promise.resolve(response.data);
      })
  },

  /**
   * Delete File
   * @param {*} params
   * @return {promise} promise
   */
  deleteFile(params) {
    return axios({
      method: 'delete',
      url: `${config.apihost}/storages/${params.name}/files/${params.file}`,
    })
  },

  /**
   * Get Storage List
   * @param {*} params
   * @return {promise} promise
   */
  getStorageList(params, cb) {
    return axios({
        method: 'get',
        url: `${config.apihost}/storages`
      })
      .then(function (response) {
        return Promise.resolve(response.data);
      })
  },

  /**
   * get List MediaContent
   * @param {*} params
   * @return {promise} promise
   */
  getListMediaContent(params) {
    return axios({
        method: 'get',
        url: `${config.apihost}/MediaContent`,
      })
      .then(function (response) {
        return Promise.resolve(response.data);
      })
  },

  /**
   * Get MediaContent
   * @param {*} params
   * @return {promise} promise
   */
  getMediaContent(params) {
    return axios({
      method: 'get',
      url: `${config.apihost}/MediaContent/${params.id}`,
    }).then((response) => {
      return Promise.resolve(response.data);
    })
  },

  /**
   * Create Content
   * @param {*} params
   * @return {promise} promise
   */
  createMediaContent(params) {
    return axios({
        method: 'post',
        url: `${config.apihost}/MediaContent`,
        data: {
          'title': params.title,
          'description': params.description,
          'pageId': params.pageId,
          'userId': params.userId,
          'contentId': params.contentId,
        }
      })
      .then(function (response) {
        let result = {
          id: response.data.id
        };
        return Promise.resolve(result);
      })
  },

  /**
   * Delete MediaContent
   * @param {*} params
   * @return {promise} promise
   */
  deleteMediaContent(params) {
    return axios({
      method: 'delete',
      url: `${config.apihost}/MediaContent/${params.id}`,
    })
  },

  /**
   * Edit MediaContent
   * @param {*} params
   * @return {promise} promise
   */
  editMediaContent(params) {
    return axios({
      method: 'put',
      url: `${config.apihost}/MediaContent/${params.id}`,
      data: {
        'title': params.title,
        'description': params.description,
        'pageId': params.pageId,
        'userId': params.userId,
        'contentId': params.contentId,
      }
    })
  },

  /**
   * Get MediaContentOfContent list
   * @param {*} params
   * @return {promise} promise
   */
  getMediaContentOfContent(params) {
    const filter = params.filter ? '?filter=' + JSON.stringify(params.filter) : ''
    return axios({
        method: 'get',
        url: `${config.apihost}/content/${params.id}/MediaContents${filter}`,
      })
      .then(function (response) {
        return Promise.resolve(response.data);
      })
  },


    /**
   * Get MediaContent Count
   * @param {*} params
   * @return {promise} promise
   */
  getMediaContentCount(params) {
    const filter = params ? '?where=' + JSON.stringify(params) : ''
    return axios({
      method: 'get',
      url: `${config.apihost}/MediaContent/count${filter}`
    }).then(function (response) {
      return Promise.resolve(response.data);
    });
  },
  
  /**
   * Get Page Count
   * @param {*} params
   * @return {promise} promise
   */
  getPageCount(params) {
    return axios({
      method: 'get',
      url: `${config.apihost}/Page/count`
    }).then(function (response) {
      return Promise.resolve(response.data);
    });
  },

  /**
   * Get User Count
   * @param {*} params
   * @return {promise} promise
   */
  getUserCount(params) {
    return axios({
      method: 'get',
      url: `${config.apihost}/Members/count`
    }).then(function (response) {
      return Promise.resolve(response.data);
    });
  },

  /**
   * get List Document
   * @param {*} params
   * @param {*} cb
   * @return {promise} promise
   */
  getListDocument(params) {
    const filter = params ? '?filter=' + JSON.stringify(params) : ''
    return axios({
        method: 'get',
        url: `${config.apihost}/Document${filter}`,
      })
      .then(function (response) {
        return Promise.resolve(response.data);
      })
  },

  /**
   * Create Document
   * @param {*} params
   * @return {promise} promise
   */
  createDocument(params, cb) {
    return axios({
        method: 'post',
        url: `${config.apihost}/Document`,
        data: {
          'name': params.name,
          'type': params.slug,
          'description': params.description,
          'url': params.url,
          'file': params.file,
          'contentId': params.contentId,
          'origName': params.origName
        }
      })
      .then(function (response) {
        let result = {
          id: response.data.id
        };
        return Promise.resolve(result);
      })
  },

  /**
   * Delete Document
   * @param {*} params
   * @return {promise} promise
   */
  deleteDocument(params, cb) {
    return axios({
      method: 'delete',
      url: `${config.apihost}/Document/${params.id}`,
    })
  },

  /**
   * Edit Document
   * @param {*} params
   * @return {promise} promise
   */
  editDocument(params, cb) {
    return axios({
      method: 'put',
      url: `${config.apihost}/Document/${params.id}`,
      data: {
        'name': params.name,
        'type': params.type,
        'description': params.description,
        'url': params.url,
        'file': params.file,
        'contentId': params.contentId,
        'origName': params.origName
      }
    })
  },

  /**
   * Get Document Count
   * @param {*} params
   * @return {promise} promise
   */
  getDocumentCount(params) {
    return axios({
      method: 'get',
      url: `${config.apihost}/Document/count`
    }).then(function (response) {
      return Promise.resolve(response.data);
    });
  },

  /**
   * Get Document
   * @param {*} params
   * @return {promise} promise
   */
  getDocument(params) {
    return axios({
      method: 'get',
      url: `${config.apihost}/Document/${params.id}`
    }).then(function (response) {
      return Promise.resolve(response.data);
    })
  },
};
