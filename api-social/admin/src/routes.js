import VueRouter from 'vue-router';
import Home from './pages/home.vue';
import ContentManager from './pages/contentmanager.vue';
import AllTextContentList from './pages/alltextcontentlist.vue';

import CreatePage from './pages/createpage.vue'
import UserEdit from './pages/useredit.vue'
import UploadFile from './components/files/upload.vue'
import Files from './components/files/files.vue'

import Pages from './pages/pages.vue'
import Users from './pages/users.vue'
import Search from './pages/search.vue'
import Page404 from './pages/404.vue'
import Profile from './pages/profile.vue'
import Login from './pages/login.vue'
import Logout from './pages/logout.vue'
import store from './store'

const ifNotAuthenticated = (to, from, next) => {
  if (!store.getters.current || (store.getters.current && !store.getters.current.isAuthenticated)) {
    next()

    return
  }
  next('/')
}

const ifAuthenticated = (to, from, next) => {
  if (store.getters.current && store.getters.current.isAuthenticated) {
    if(store.getters.current.user){
        next();
    } else {
        store.dispatch("getCurrentUser").then(()=>{
            next();
        }).catch((error) => {
            console.log(error);
            next('/login')
        });
    }

    return
  }
  next('/login')
}


const routes = new VueRouter({
    mode: 'history',
    scrollBehavior: (to, from, savedPosition) => {
        if (savedPosition) {
            // savedPosition is only available for popstate navigations.
            return savedPosition
        } else {
            const position = {}
                // new navigation.
                // scroll to anchor by returning the selector
            if (to.hash) {
                position.selector = to.hash

                // specify offset of the element
                if (to.hash === '#anchor2') {
                    position.offset = { y: 100 }
                }
            }
            // check if any matched route config has meta that requires scrolling to top
            if (to.matched.some(m => m.meta.scrollToTop)) {
                // cords will be used if no selector is provided,
                // or if the selector didn't match any element.
                position.x = 0
                position.y = 0
            }
            // if the returned position is falsy or an empty object,
            // will retain current scroll position.
            return position
        }
    },
    routes: [{
            path: '/',
            name: 'home',
            component: Home,
            props: true,
            meta: {
                scrollToTop: true,
            },
            beforeEnter: ifAuthenticated
        },
        {
            path: '/login',
            name: 'Login',
            component: Login,
            props: true,
            beforeEnter: ifNotAuthenticated
        },
        {
            path: '/logout',
            name: 'Logout',
            component: Logout,
            props: true,
            beforeEnter: ifAuthenticated
        },
        {
            path: '/alltextcontents',
            name: 'alltextcontentlist',
            component: AllTextContentList,
            props: (route) => ({
                params: {
                    isEntry: false
                }
            }),
            beforeEnter: ifAuthenticated
        },
        {
            path: '/contents',
            name: 'contentlist',
            component: ContentManager,
            props: (route) => ({
                params: {
                    contentType: 'content',
                    command: 'list',
                    isEntry: false
                }
            }),
            beforeEnter: ifAuthenticated
        },
        {
            path: '/contents/new',
            name: 'contentcreate',
            component: ContentManager,
            props: (route) => ({
                params: {
                    contentType: 'content',
                    command: 'edit',
                    isEntry: false
                }
            }),
            beforeEnter: ifAuthenticated
        },
        {
            path: '/contents/edit/:id',
            name: 'contentedit',
            component: ContentManager,
            props: (route) => ({
                params: {
                    id: route.params.id,
                    contentType: 'content',
                    command: 'edit',
                    isEntry: false
                }
            }),
            beforeEnter: ifAuthenticated
        },
        {
            path: '/contents/entry/:contentSlug/:command?/:id?',
            name: 'customcontentedit',
            component: ContentManager,
            props: (route) => ({
                params: {
                    id: route.params.id,
                    contentSlug: route.params.contentSlug ? route.params.contentSlug : 'all',
                    command: route.params.command,
                    isEntry: true
                }
            }),
            beforeEnter: ifAuthenticated
        },
        {
            path: '/createPage',
            name: 'createPage',
            component: CreatePage,
            props: true,
            beforeEnter: ifAuthenticated
        },
        {
            path: '/search',
            name: 'search',
            component: Search,
            props: true,
            beforeEnter: ifAuthenticated
        },
        {
            path: '/profile',
            name: 'profile',
            component: Profile,
            props: true,
            beforeEnter: ifAuthenticated
        },
        {
            path: '/user/edit/:id',
            name: 'useredit',
            component: UserEdit,
            props: (route) => ({
                params: {
                    id: route.params.id,
                    isEntry: false
                }
            }),
            beforeEnter: ifAuthenticated
        },
        {
            path: '/files',
            name: 'files',
            component: Files,
            props: true,
            beforeEnter: ifAuthenticated
        },
        {
            path: '/uploadFile',
            name: 'uploadFile',
            component: UploadFile,
            props: true,
            beforeEnter: ifAuthenticated
        },
        {
            path: '/pages',
            name: 'pages',
            component: Pages,
            props: true,
            beforeEnter: ifAuthenticated
        },
        {
            path: '/users',
            name: 'users',
            component: Users,
            props: true,
            beforeEnter: ifAuthenticated
        },
        {
            path: '/user/:id',
            name: 'event',
            component: Event,
            props: true,
            beforeEnter: ifAuthenticated
        },
        {
            path: '*',
            name: 'error',
            component: Page404,
        },
    ],
});

export default routes;
