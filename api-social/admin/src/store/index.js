import Vue from 'vue';
import Vuex from 'vuex';
import TextContents from './modules/textcontents';
import MediaContents from './modules/mediacontents';
import Contents from './modules/contents';
import Pages from './modules/pages';
import User from './modules/user';
import Files from './modules/files';
import Members from './modules/members';
import Documents from './modules/documents';
import Videos from './modules/videos'
import Banners from './modules/banners'
import Pagination from './../components/pagination.vue'

Vue.use(Vuex);

const debug = process.env.NODE_ENV !== 'production';

export default new Vuex.Store({
    modules: {
        TextContents,
        MediaContents,
        Contents,
        Pages,
        User,
        Members,
        Files,
        Pagination,
        Documents,
        Videos,
        Banners
    },
    strict: debug,
});