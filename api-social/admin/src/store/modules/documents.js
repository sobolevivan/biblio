import api from '../../api/api';
import { resolve } from 'q';

// initial state
const state = {
  documents: [],
  document: null,
  documentCount: 0
};

// getters
const getters = {
  documents: (state) => {
    return state.documents;
  },
  document: (state) => {
    return state.document;
  },
  documentCount: (state) => {
    return state.documentCount;
  }
};

// actions
const actions = {
  getListDocument({commit}, params) {
    return api.getListDocument(params).then((documents) => {
      commit('setDocuments', documents);
    });
  },
  getDocumentCount({commit}, params) {
    return api.getDocumentCount(params).then((documents) => {
      commit('setDocumentCount', documents);
      return Promise.resolve(documents);
    });
  },
  createDocument({commit}, params) {
    return api.createDocument(params).then((document) => {
      commit('setDocument', document);
    });
  },
  getDocument({commit}, params) {
    return api.getDocument(params).then((document) => {
      commit('setDocument', document);
    });
  },
  deleteDocument({commit}, params) {
    return api.deleteDocument(params).then((documents) => {
      commit('setDocuments', (state.documents || []).filter(mc => mc.id !== params.id));
    });
  },
  editDocument({commit}, params) {
    return api.editDocument(params).then((documents) => {
      commit('setDocuments', documents);
    });
  }
};

// mutations
const mutations = {
  setDocuments(state, documents) {
    state.documents = documents;
  },
  setDocument(state, item) {
    state.document = item;
  },
  setDocumentCount(state, item) {
    state.documentCount = item;
  }
};

export default {
  state,
  getters,
  actions,
  mutations,
};
