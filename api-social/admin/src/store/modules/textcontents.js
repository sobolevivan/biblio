import api from '../../api/api';

// initial state
const state = {
  textContents: [],
  textContent: null,
  textContentCount: 0
};

// getters
const getters = {
  textContents: (state) => {
    return state.textContents;
  },
  textContent: (state) => {
    return state.textContent;
  },
  textContentCount: (state) => {
    return state.textContentCount;
  }
};

// actions
const actions = {
  getListTextContent({commit}, params) {
    return api.getListTextContent(params).then((textContents) => {
      commit('setTextContents', textContents);
    });
  },
  getTextContentCount({commit}, params) {
    return api.getTextContentCount(params).then((textContents) => {
      commit('setTextContentCount', textContents);
      return Promise.resolve(textContents);
    });
  },
  getTextContentOfContent({commit}, params) {
    return api.getTextContentOfContent(params).then((textContents) => {
      commit('setTextContents', textContents);
    });
  },
  getTextContent({commit}, params) {
    return api.getTextContent(params).then((textContent) => {
      commit('setTextContent', textContent);
    });
  },
  createTextContent({commit}, params) {
    return api.createTextContent(params).then((textContent) => {
      commit('setTextContent', textContent);
    });
  },
  deleteTextContent({commit}, params) {
    return api.deleteTextContent(params).then((result) => {
      commit('setTextContents', (state.textContents || []).filter(tc => tc.id !== params.id));
    });
  },
  editTextContent({commit}, params) {
    return api.editTextContent(params).then((textContent) => {
      commit('setTextContent', textContent);
    });
  },
  clearTextContent({commit}) {
    commit('setTextContent', null);
  }
};

// mutations
const mutations = {
  setTextContents(state, textContents) {
    state.textContents = textContents;
  },
  setTextContent(state, item) {
    state.textContent = item;
  },
  setTextContentCount(state, item) {
    state.textContentCount = item;
  }
};

export default {
  state,
  getters,
  actions,
  mutations,
};
