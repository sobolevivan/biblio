import api from '../../api/api';

// initial state
const state = {
  users: [],
  user: null,
  auth: null,
  userCount: 0
};

// getters
const getters = {
  users: (state) => {
    return state.users;
  },
  user: (state) => {
    return state.user;
  },
  userCount: (state) => {
    return state.userCount;
  },
};

// actions
const actions = {
  getUser({commit}, params) {
    return api.getUser(params).then((user) => {
      commit('setUser', user);
    });
  },
  getListUser({commit}, params) {
    return api.getListUser(params).then((users) => {
      commit('setUsers', users);
    });
  },
  getUserCount({commit}, params) {
    return api.getUserCount(params).then((users) => {
      commit('setUserCount', users);
      return Promise.resolve(users)
    });
  },
  createUser({commit}, params) {
    return api.createUser(params).then((user) => {
      commit('setUser', user);
    });
  },
  deleteUser({commit}, params) {
    return api.deleteUser(params).then((users) => {
      commit('setUser', user);
    });
  },
  editUser({commit}, params) {
    return api.editUser(params).then((user) => {
      commit('setUser', user);
    });
  },
  editProfile({commit}, params) {
    return api.editProfile(params).then((profile) => {
      commit('setUserProfile', profile);
    });
  },
  createProfile({commit}, params) {
    return api.createProfile(params).then((profile) => {
      commit('setUserProfile', profile);
    });
  }
};

// mutations
const mutations = {
  setUsers(state, users) {
    state.users = users;
  },
  setUser(state, user) {
    state.user = user;
  },
  setUserCount(state, user) {
    state.userCount = user;
  },
  setUserProfile(state, profile) {
    state.user = {...state.user, profile};
  }
};

export default {
  state,
  getters,
  actions,
  mutations,
};
