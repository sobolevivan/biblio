import api from '../../api/api';

// initial state
const state = {
  contents: [],
  content: null,
  contentCount: null
};

// getters
const getters = {
  contents: (state) => {
    return state.contents;
  },
  content: (state) => {
    return state.content;
  },
  contentCount: (state) => {
    return state.contentCount;
  }
};

// actions
const actions = {
  getListContent({commit}, params) {
    return api.getListContent(params).then((contents) => {
      commit('setContents', contents);
    });
  },
  getContent({commit}, params) {
    return api.getContent(params).then((content) => {
      commit('setContent', content);
    });
  },
  getContentBySlug({commit}, params) {
    return api.getContentBySlug(params).then((content) => {
      commit('setContent', content);
      return Promise.resolve(content);
    });
  },
  getContentCount({commit}, params) {
    return api.getContentCount(params).then((contentCount) => {
      commit('setContentCount', contentCount);
      return Promise.resolve(contentCount);
    });
  },
  createContent({commit}, params) {
    return api.createContent(params).then((content) => {
      commit('setContent', content);
    });
  },
  deleteContent({commit}, params) {
    return api.deleteContent(params).then(() => {
      commit('setContents', (state.contents || []).filter(item => item.id !== params.id));
    });
  },
  editContent({commit}, params) {
    return api.editContent(params).then((content) => {
      commit('setContent', content);
    });
  }
};

// mutations
const mutations = {
  setContents(state, contents) {
    state.contents = contents;
  },
  setContent(state, content) {
    state.content = content;
  },
  setContentCount(state, contentCount) {
    state.contentCount = contentCount;
  }
};

export default {
  state,
  getters,
  actions,
  mutations,
};
