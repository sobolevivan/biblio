import api from '../../api/api';

// initial state
const state = {
  mediaContents: [],
  mediaContent: null,
  mediaContentCount: 0
};

// getters
const getters = {
  mediaContents: (state) => {
    return state.mediaContents;
  },
  mediaContent: (state) => {
    return state.mediaContent;
  }
  ,
  mediaContentCount: (state) => {
    return state.mediaContentCount;
  }
};

// actions
const actions = {
  getListMediaContent({commit}, params) {
    return api.getListMediaContent(params).then((mediaContents) => {
      console.log("getListMediaContent:", mediaContents);
      commit('setMediaContents', mediaContents);
    });
  },
  getMediaContentOfContent({commit}, params) {
    return api.getMediaContentOfContent(params).then((mediaContents) => {
      commit('setMediaContents', mediaContents);
    });
  },
  getMediaContentCount({commit}, params) {
    return api.getMediaContentCount(params).then((mediaContents) => {
      commit('setMediaContentCount', mediaContents);
      return Promise.resolve(mediaContents)
    });
  },
  getMediaContent({commit}, params) {
    return api.getMediaContent(params).then((mediaContent) => {
      commit('setMediaContent', mediaContent);
    });
  },
  createMediaContent({commit}, params) {
    return api.createMediaContent(params).then((mediaContent) => {
      commit('setMediaContent', mediaContent);
    });
  },
  deleteMediaContent({commit}, params) {
    return api.deleteMediaContent(params).then((result) => {
      commit('setMediaContents', (state.mediaContents || []).filter(mc => mc.id !== params.id));
    });
  },
  editMediaContent({commit}, params) {
    return api.editMediaContent(params).then((mediaContent) => {
      commit('setMediaContent', mediaContent);
    });
  }
};

// mutations
const mutations = {
  setMediaContents(state, mediaContents) {
    state.mediaContents = mediaContents;
  },
  setMediaContent(state, item) {
    state.mediaContent = item;
  },
  setMediaContentCount(state, item) {
    state.mediaContentCount = item;
  }
  
};

export default {
  state,
  getters,
  actions,
  mutations,
};
