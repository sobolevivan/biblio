import api from '../../api/api';
import { resolve } from 'q';

// initial state
const state = {
  videos: [],
  video: null,
  videoCount: 0
};

// getters
const getters = {
  videos: (state) => {
    return state.videos;
  },
  video: (state) => {
    return state.video;
  },
  videoCount: (state) => {
    return state.videoCount;
  }
};

// actions
const actions = {
  getListVideo({commit}, params) {
    return api.getListVideo(params).then((videos) => {
      commit('setVideos', videos);
    });
  },
  getVideoCount({commit}, params) {
    return api.getVideoCount(params).then((videos) => {
      commit('setVideoCount', videos);
      return Promise.resolve(videos);
    });
  },
  createVideo({commit}, params) {
    return api.createVideo(params).then((video) => {
      commit('setVideo', video);
    });
  },
  deleteVideo({commit}, params) {
    return api.deleteVideo(params).then((videos) => {
      commit('setVideo', videos);
    });
  },
  editVideo({commit}, params) {
    return api.editVideo(params).then((videos) => {
      commit('setVideos', videos);
    });
  }
};

// mutations
const mutations = {
  setVideos(state, videos) {
    state.videos = videos;
  },
  setVideo(state, item) {
    state.video = item;
  },
  setVideoCount(state, item) {
    state.videoCount = item;
  }
};

export default {
  state,
  getters,
  actions,
  mutations,
};
