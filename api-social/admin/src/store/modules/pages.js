import api from '../../api/api';
import { resolve } from 'q';

// initial state
const state = {
  pages: [],
  page: null,
  pageCount: 0
};

// getters
const getters = {
  pages: (state) => {
    return state.pages;
  },
  page: (state) => {
    return state.page;
  },
  pageCount: (state) => {
    return state.pageCount;
  }
};

// actions
const actions = {
  getListPage({commit}, params) {
    return api.getListPage(params).then((pages) => {
      commit('setPages', pages);
    });
  },
  getPageCount({commit}, params) {
    return api.getPageCount(params).then((pages) => {
      commit('setPageCount', pages);
      return Promise.resolve(pages);
    });
  },
  createPage({commit}, params) {
    return api.createPage(params).then((page) => {
      commit('setPage', page);
    });
  },
  deletePage({commit}, params) {
    return api.deletePage(params).then((pages) => {
      commit('setPages', pages);
    });
  },
  editPage({commit}, params) {
    return api.editPage(params).then((pages) => {
      commit('setPages', pages);
    });
  }
};

// mutations
const mutations = {
  setPages(state, pages) {
    state.pages = pages;
  },
  setPage(state, item) {
    state.page = item;
  },
  setPageCount(state, item) {
    state.pageCount = item;
  }
};

export default {
  state,
  getters,
  actions,
  mutations,
};
