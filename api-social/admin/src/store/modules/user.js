import api from '../../api/api';
import axios from 'axios';
// initial state
const state = {
  user: null,
  isAuthenticated: null,
  ttl: 0,
  userId: null,
  accessToken: null,
};

// getters
const getters = {
  current: (state) => {
    return {
      user: state.user,
      userId: state.userId || localStorage.getItem('userId'),
      accessToken: state.accessToken || localStorage.getItem('accessToken'),
      ttl: state.ttl || localStorage.getItem('ttl'),
      isAuthenticated: state.isAuthenticated || localStorage.getItem('isAuthenticated')
    }
  }
};

// actions
const actions = {
  login({commit}, params) {
    return api.auth(params).then((auth) => {
      commit('setAuthData', auth);
    });
  },

  logout({commit}) {
    commit('clearAuthData');
  },

  getCurrentUser({commit}) {

      axios.defaults.params = {};
      axios.defaults.params[ 'access_token' ] = this.getters.current.accessToken;

      return api.getUser({
        userId: this.getters.current.userId
      }).then((user) => {
        commit('setCurrentUser', user);
      })
  },
};

// mutations
const mutations = {
  setCurrentUser(state, user) {
    state.user = user;
  },

  clearAuthData(state) {
    localStorage.removeItem('isAuthenticated', false);
    localStorage.removeItem('accessToken', null);
    localStorage.removeItem('userId', null);
    localStorage.removeItem('ttl', null);

    state.user = null;
  },

  setAuthData(state, auth) {
    localStorage.setItem('isAuthenticated', true);
    localStorage.setItem('accessToken', auth.id);
    localStorage.setItem('userId', auth.userId);
    localStorage.setItem('ttl', auth.ttl);

    state.isAuthenticated = true;
    state.ttl = auth.ttl;
    state.userId = auth.userId;
    state.accessToken = auth.id;
  }
};

export default {
  state,
  getters,
  actions,
  mutations,
};
