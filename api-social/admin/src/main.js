import Vue from 'vue';
import routes from './routes';
import store from './store';
import VueRouter from 'vue-router';
import App from './App';
import VueScrollTo from 'vue-scrollto';
import Vuetify from 'vuetify';
import Vueditor from 'vueditor';
import VueSelectImage from 'vue-select-image'

require('vue-select-image/dist/vue-select-image.css')
import 'vueditor/dist/style/vueditor.min.css';

var moment = require('moment');
moment().format('YY-MM-DD');

Vue.use(Vuetify);
Vue.use(VueSelectImage)

// vueditor config https://github.com/hifarer/vueditor
let config = {
    toolbar: [
      'removeFormat', 'undo', '|', 'elements', 'fontName', 'fontSize', 'foreColor', 'backColor', 'divider',
      'bold', 'italic', 'underline', 'strikeThrough', 'links', 'divider', 'subscript', 'superscript',
      'divider', 'justifyLeft', 'justifyCenter', 'justifyRight', 'justifyFull', '|', 'indent', 'outdent',
      'insertOrderedList', 'insertUnorderedList', '|', 'picture', 'tables', '|', 'switchView'
    ],
    fontName: [
      {val: 'arial black'},
      {val: 'times new roman'},
      {val: 'Courier New'}
    ],
    fontSize: [
      '12px', '14px', '16px', '18px', '20px', '24px', '28px', '32px', '36px'
    ],
    uploadUrl: '',
    id: '',
    classList: []
};

Vue.use(Vueditor, config);

// You can also pass in the default options
Vue.use(VueScrollTo, {
     container: 'body',
     duration: 1000,
     easing: 'ease',
     offset: 0,
     force: true,
     cancelable: true,
     onStart: false,
     onDone: false,
     onCancel: false,
     x: false,
     y: true,
 });

Vue.use(VueRouter);
Vue.prototype.$globalBus = new Vue();

const app = new Vue({
    el: '#app',
    components: {
        App,
    },
    mounted() {
    },
    store,
    router: routes,
    template: '<App/>',
});