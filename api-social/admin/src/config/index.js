export default {
  apihost: 'http://localhost:3001/api',
  imagehost: 'http://localhost:3001/images',
  storage: 'http://localhost:3001/api/Storages',
  sites: [{
    id: 1,
    name: 'Biblio',
    slug: 'social'
  }]
}
