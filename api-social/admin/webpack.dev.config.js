const path = require('path');
const webpack = require('webpack');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const WriteFilePlugin = require('write-file-webpack-plugin');
const MinifyPlugin = require('babel-minify-webpack-plugin');
//const WebpackFileChanger = require('webpack-file-changer');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const DIST_PATH_STATIC = path.resolve(__dirname, '..', './client/static/public');
const DIST_PATH_ROOT = path.resolve(__dirname, '..', './client');

const devMode = process.env.NODE_ENV !== 'production';

module.exports = {
  node: {
    fs: 'empty'
  },
  context: __dirname,
  entry: [
    './src/main.js',
    './src/assets/styles/main.scss'
  ],
  devServer: {
    compress: true,
    port: 8000,
    index: 'index.html',
    publicPath: '/',
    historyApiFallback: true,
  },
  resolve: {
    modules: [
      'node_modules'
    ],
    alias: {
      'main': path.resolve(__dirname, 'src/main.js'),
      'vue$': 'vue/dist/vue.esm.js'
    },
    extensions: ['*', '.js', '.vue', '.json'],
  },
  output: {
    path: DIST_PATH_STATIC,
    filename: '[name].bundle.js'
  },
  plugins: [
    new webpack.ProvidePlugin({
      'window.Quill': 'quill'
    })
  ],
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules(?!\/quill-image-drop-module|quill-image-resize-module)/,
        loader: 'babel-loader'
      },
      {
        test: /\.vue$/,
        loader: 'vue-loader',
        exclude: [
          /(node_modules|bower_components)/
        ]
      },
      {
        test: /\.js$/,
        loader: 'babel-loader',
        exclude: [
          /(node_modules|bower_components)/
        ]
      },
      { // regular css files
        test: /\.css$/,
        use: ExtractTextPlugin.extract({
          use: 'css-loader?importLoaders=1',
        }),
      },
      {
        test: /\.scss$/,
        use: ExtractTextPlugin.extract({
          fallback: 'style-loader',
          use: [
            {
                loader: 'css-loader',
                loader: 'css-loader?-url,minimize,sourceMap',
                //options: {
                    // If you are having trouble with urls not resolving add this setting.
                    // See https://github.com/webpack-contrib/css-loader#url
                //    url: false,
                //    minimize: true,
                //    sourceMap: true,
                //},
            },
            {
                loader: 'sass-loader?sourceMap',
                //options: {
                //  sourceMap: true,
                //},
            },
          ],
        }),
      },
      {
        test: /\.(png|jpg|gif|svg)$/,
        loader: 'file-loader?name=[name].[ext]',
        //options: {
        //  name: '[name].[ext]',
        //},
      },
    ],
  },

  performance: {
    hints: false,
  },
  devtool: '#eval-source-map',
  stats: 'verbose',
};

if (process.env.NODE_ENV === 'production') {
  module.exports.devtool = '#source-map';
  module.exports.plugins = (module.exports.plugins || []).concat([
    /* new webpack.optimize.UglifyJsPlugin({
      sourceMap: true,
      compress: {
        warnings: false,
      },
    }) ,*/
    new WriteFilePlugin(),
    new CopyWebpackPlugin([
      {from: 'index.html', to: './../client'},
    ]),
    new webpack.LoaderOptionsPlugin({
      minimize: true,
    }),
  ]);
} else {
  module.exports.plugins = (module.exports.plugins || []).concat([
    new ExtractTextPlugin({
      filename: './assets/styles/style.css',
      allChunks: true,
    }),
    // new MinifyPlugin(),
    new WriteFilePlugin(),
    new CopyWebpackPlugin([
      {from: 'index.html', to: DIST_PATH_ROOT},
    ]),
  ]);
}